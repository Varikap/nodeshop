const express = require('express');
const app = express();

// like filter, every request passes this 
app.use((req, res, next) => {
    res.status(200).json({
        message: 'Test'
    });
});

module.exports = app;